package webService01

import org.scalatra._

// JSON-related libraries
import org.json4s.{DefaultFormats, Formats}

// JSON handling support from Scalatra
import org.scalatra.json._

class MyScalatraServlet extends ScalatraServlet with JacksonJsonSupport {

  // A Flower object to use as a faked-out data model
  case class Flower(message: String)

  // Sets up automatic case class to JSON output serialization, required by
  // the JValueResult trait.
  protected implicit lazy val jsonFormats: Formats = DefaultFormats

  // Before every action runs, set the content type to be in JSON format.
  before() {
    contentType = formats("json")
  }

  get("/hello/:name") {
    object FlowerData {
      // Some fake flowers data so we can simulate retrievals.
      var all = List(
          Flower("Hello " + {params("name")}))
    }
    FlowerData.all
  }
}
