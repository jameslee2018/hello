package webService01

import org.scalatra.test.scalatest._

class MyScalatraServletTests extends ScalatraFunSuite {
  addServlet(classOf[MyScalatraServlet], "/*")

  test("GET /hello/:name on MyScalatraServlet should return status 200,and content contains 'Hello :name' "){
    get("/hello/James"){
      status should equal (200)
      body should include ("{\"message\":\"Hello James\"}")
    }
  }
}
