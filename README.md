# README #

This is the hello project I made.

Please execute the source following the orders:

1.  download all the source.
2.  execute 
    2.1   cd /your/project/directory
	2.2   sbt
	2.3     >jetty:start
3.  Now open browser and use http://localhost:8080/hello/:name to see the output on the webpage.
4.  at the end, use the command to finish the program
    4.1     >jetty:stop
	4.2   exit

Thanks very much.

